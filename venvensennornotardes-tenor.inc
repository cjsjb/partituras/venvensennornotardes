\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key d \major

		R1*2  |
		fis 2 \times 2/3 { fis 4 e 8 d 4 b, 8 }  |
		a, 2 d 4 r  |
%% 5
		e 8 e 4 -\staccato e 8 d 4 e  |
		fis 4 ( e ) d r  |
		a 2 \times 2/3 { a 4 g 8 fis 4 e 8 }  |
		d 2 fis 4 -\staccato fis  |
		g 4 ( b ) a g  |
%% 10
		fis 2. r8 fis  |
		a 8 a 4 a 8 fis 4 a  |
		b 4 ( a ) fis r8 fis  |
		e 8 e 4 e 8 d 4 e  |
		fis 2 r4 r8 fis  |
%% 15
		a 8 a 4 a 8 fis 4 a  |
		b 4 ( a ) fis r8 fis  |
		e 8 e 4 e 8 d 4 cis  |
		d 2. r4  |
		fis 2 \times 2/3 { fis 4 e 8 d 4 b, 8 }  |
%% 20
		a, 2 d 4 r  |
		e 8 e 4 -\staccato e 8 d 4 e  |
		fis 4 ( e ) d r  |
		a 2 \times 2/3 { a 4 g 8 fis 4 e 8 }  |
		d 2 fis 4 -\staccato fis  |
%% 25
		g 4 ( b ) a g  |
		fis 2 r4 fis  |
		g 4 ( b ) a g  |
		fis 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor.

		El mun -- do mue -- re de frí -- o,
		el al -- ma per -- "dió el" ca -- lor,
		los hom -- bres no son her -- ma -- nos,
		el mun -- do no tie -- "ne a" -- mor.

		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor,
		ven pron -- to, Se -- ñor.
	}
>>
