\context ChordNames
	\chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d1 d1

	% ven, ven, sennor, no tardes...
	d1 d1 a1 d1
	d1 d1 e2:m a2 d1

	% el mundo muere de frio...
	d1 d1 a1 d1
	d1 d1 e2:m a2 d1

	% ven, ven, sennor, no tardes...
	d1 d1 a1 d1
	d1 d1 e2:m a2 d1
	e2:m a2 d1
	d1
	}
