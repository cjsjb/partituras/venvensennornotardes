\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1*2  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 r  |
%% 5
		e' 8 e' 4 -\staccato e' 8 d' 4 e'  |
		fis' 4 ( e' ) d' r  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 -\staccato d'  |
		e' 4 ( g' ) fis' e'  |
%% 10
		d' 2. r8 fis'  |
		a' 8 a' 4 a' 8 fis' 4 a'  |
		b' 4 ( a' ) fis' r8 fis'  |
		e' 8 e' 4 e' 8 d' 4 e'  |
		fis' 2 r4 r8 fis'  |
%% 15
		a' 8 a' 4 a' 8 fis' 4 a'  |
		b' 4 ( a' ) fis' r8 fis'  |
		e' 8 e' 4 e' 8 d' 4 cis'  |
		d' 2. r4  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
%% 20
		a 2 d' 4 r  |
		e' 8 e' 4 -\staccato e' 8 d' 4 e'  |
		fis' 4 ( e' ) d' r  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 -\staccato d'  |
%% 25
		e' 4 ( g' ) fis' e'  |
		d' 2 r4 d'  |
		e' 4 ( g' ) fis' e'  |
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor.

		El mun -- do mue -- re de frí -- o,
		el al -- ma per -- "dió el" ca -- lor,
		los hom -- bres no son her -- ma -- nos,
		el mun -- do no tie -- "ne a" -- mor.

		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor,
		ven pron -- to, Se -- ñor.
	}
>>
