\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"
		\key d \major

		R1*2  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 r  |
%% 5
		cis' 8 cis' 4 -\staccato cis' 8 b 4 cis'  |
		d' 4 ( cis' ) a r  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 -\staccato d'  |
		e' 4 ( g' ) fis' e'  |
%% 10
		d' 2. r8 d'  |
		fis' 8 fis' 4 fis' 8 d' 4 fis'  |
		g' 4 ( fis' ) d' r8 d'  |
		cis' 8 cis' 4 cis' 8 b 4 cis'  |
		d' 2 r4 r8 d'  |
%% 15
		fis' 8 fis' 4 fis' 8 d' 4 fis'  |
		g' 4 ( fis' ) d' r8 d'  |
		cis' 8 cis' 4 cis' 8 b 4 a  |
		a 2. r4  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
%% 20
		a 2 d' 4 r  |
		cis' 8 cis' 4 -\staccato cis' 8 b 4 cis'  |
		d' 4 ( cis' ) a r  |
		fis' 2 \times 2/3 { fis' 4 e' 8 d' 4 b 8 }  |
		a 2 d' 4 -\staccato d'  |
%% 25
		e' 4 ( g' ) fis' e'  |
		d' 2 r4 d'  |
		e' 4 ( g' ) fis' e'  |
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor.

		El mun -- do mue -- re de frí -- o,
		el al -- ma per -- "dió el" ca -- lor,
		los hom -- bres no son her -- ma -- nos,
		el mun -- do no tie -- "ne a" -- mor.

		Ven, ven, Se -- ñor, no tar -- des,
		ven, ven, que "te es" -- pe -- ra -- mos.
		Ven, ven, Se -- ñor, no tar -- des,
		ven pron -- to, Se -- ñor,
		ven pron -- to, Se -- ñor.
	}
>>
